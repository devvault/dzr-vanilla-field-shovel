modded class ActionRepairPart
{
	override protected void SetBuildingAnimation( ItemBase item )
	{
		if (item.Type() == dzr_FieldShovel)
		{
			m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_DIG;
		} 
		else 
		{
			super.SetBuildingAnimation( item );
		}
	}
}
